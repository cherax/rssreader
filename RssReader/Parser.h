//
//  Parser.h
//  Test1
//
//  Created by Przemek Jaskot on 20.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parser : NSObject <NSXMLParserDelegate>
{
    NSXMLParser *parser;
    NSMutableString *element;
}

@property(strong, nonatomic) NSMutableArray *titles;
@property(strong, nonatomic) NSMutableArray *links;

-(void) refreshData;

@end
