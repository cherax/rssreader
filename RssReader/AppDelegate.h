//
//  AppDelegate.h
//  Test1
//
//  Created by Przemek Jaskot on 20.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;
@class WebViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    MainViewController *mainViewController;
    WebViewController *webViewController;
    UINavigationController *mainNavigationController;
    UINavigationController *webNavigationController;
}

@property (strong, nonatomic) UIWindow *window;

//@property (strong, nonatomic) MainViewController *mainViewController;
//@property (strong, nonatomic) WebViewController *webViewController;

- (void) showWebView:(NSString *)link;
- (void) showMainView;

@end
