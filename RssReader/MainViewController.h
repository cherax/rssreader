//
//  MainViewController.h
//  Test1
//
//  Created by Przemek Jaskot on 20.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Parser.h"

#import "AppDelegate.h"

@interface MainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    float screenW;
    float screenH;
    float statusBarH;
    float navigationBarH;
    UITableView *table;
    Parser *parser;
}

@end
