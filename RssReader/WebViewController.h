//
//  WebViewController.h
//  Test1
//
//  Created by Przemek Jaskot on 20.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

@interface WebViewController : UIViewController <UIWebViewDelegate>
{
    float screenW;
    float screenH;
    float statusBarH;
    float navigationBarH;
    UIWebView *webview;
}

-(void)loadSite:(NSString *)link;

@end
