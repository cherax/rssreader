//
//  Parser.m
//  Test1
//
//  Created by Przemek Jaskot on 20.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import "Parser.h"

@implementation Parser

-(id)init
{
    if(self == [super init])
    {
        _titles = [[NSMutableArray alloc] init];
        _links = [[NSMutableArray alloc] init];
        element = [[NSMutableString alloc] init];
        
        NSURL *url = [NSURL URLWithString:@"http://www.ps3site.pl/feed"];
        parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        [parser setDelegate:self];
        [parser parse];
    }
    return self;
}

//----------------------------------------------------

-(void) refreshData
{
    [_titles removeAllObjects];
    [_links removeAllObjects];
    [parser parse];
}

//----------------------------------------------------

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    element = [NSMutableString string];
}

//----------------------------------------------------

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if( [elementName isEqualToString:@"title"] )
    {
        [_titles addObject:element];
    }
    else if ( [elementName isEqualToString:@"link"] )
    {
        [_links addObject:element];
    }
}

//----------------------------------------------------

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [element appendString:string];
}

//----------------------------------------------------

@end
