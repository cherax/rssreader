//
//  WebViewController.m
//  Test1
//
//  Created by Przemek Jaskot on 20.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

//----------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    screenW = [UIScreen mainScreen].bounds.size.width;
    screenH = [UIScreen mainScreen].bounds.size.height;
    statusBarH = [UIApplication sharedApplication].statusBarFrame.size.height;
    navigationBarH = self.navigationController.navigationBar.bounds.size.height;
    
    [self customizeNavigationBar];
    [self createWebView];
}

//----------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//----------------------------------------------------------------

-(void) customizeNavigationBar
{
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(pressedBack)];
    self.navigationItem.leftBarButtonItem = btn;
}

//----------------------------------------------------------------

-(void)pressedBack
{
    AppDelegate *myDelegate = [[UIApplication sharedApplication] delegate];
    [myDelegate showMainView];
}

//----------------------------------------------------------------

-(void) createWebView
{
    webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH - statusBarH - navigationBarH)];
    [webview setDelegate:self];
    [self.view addSubview:webview];
}

//----------------------------------------------------------------

-(void) loadSite:(NSString *)link
{
    NSLog(@"%@", link);
    NSURL *url = [NSURL URLWithString:link];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webview loadRequest:request];
}

//----------------------------------------------------------------

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Loading error");
}

//----------------------------------------------------------------

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"Start");
}

//----------------------------------------------------------------

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"Finish");
}

@end
