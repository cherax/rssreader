//
//  MainViewController.m
//  Test1
//
//  Created by Przemek Jaskot on 20.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

//--------------------------------------------------------------

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    screenW = [UIScreen mainScreen].bounds.size.width;
    screenH = [UIScreen mainScreen].bounds.size.height;
    statusBarH = [UIApplication sharedApplication].statusBarFrame.size.height;
    navigationBarH = self.navigationController.navigationBar.bounds.size.height;
    
    parser = [[Parser alloc] init];
    
    [self customizeNavigationBar];
    [self createTable];
}

//--------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//--------------------------------------------------------------

-(void) customizeNavigationBar
{
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(pressedRefresh)];
    self.navigationItem.rightBarButtonItem = btn;
}

//--------------------------------------------------------------

-(void)pressedRefresh
{
    [parser refreshData];
    [table reloadData];
}

//--------------------------------------------------------------

-(void) createTable
{
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH - statusBarH - navigationBarH) style:UITableViewStylePlain];
    [table setDelegate:self];
    [table setDataSource:self];
    [self.view addSubview:table];
}

//--------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [parser.titles count];
}

// ----------------------------------------------------------------------

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    cell.textLabel.text = [parser.titles objectAtIndex:indexPath.row];
    return cell;
}

// ----------------------------------------------------------------------

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *link = [parser.links objectAtIndex:indexPath.row];
    //NSString *link = @"http://www.google.com";
    
    AppDelegate *myDelegate = [[UIApplication sharedApplication] delegate];
    [myDelegate showWebView:link];
}

//--------------------------------------------------------------

@end
